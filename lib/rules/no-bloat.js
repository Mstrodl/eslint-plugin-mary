exports.meta = {
  docs: {
    description: 'prevent bloated library usage',
    category: 'Performance',
    recommended: true,
  },
  schema: [],
}

const FORBIDDEN_SECTORS = [
  '@angular',
]

const FORBIDDEN_FRUITS = [
  'react',
  'react-dom',
  'react-native',

  'vue',

  'angular',
  '@angular/core',
  '@angular/cli',
]

function isForbidden (name) {
  if (FORBIDDEN_FRUITS.includes(name)) {
    return true
  }

  const firstToken = name.substring(0, name.indexOf("/"));
  if (
    firstToken.length &&
    (
      FORBIDDEN_FRUITS.includes(firstToken) ||
      FORBIDDEN_SECTORS.includes(firstToken)
    )
  ) {
    return true
  }

  return false
}

exports.create = function (context) {
  return {
    CallExpression (node) {
      if (
        node.callee.type !== 'Identifier' ||
        node.callee.name !== 'require'
      ) {
        return
      }

      const { arguments: args } = node

      for (const arg of args) {
        if (arg.type !== 'Literal') {
          continue
        }

        const path = arg.value
        const packageName = path.split('/')[0].replace(/@/g, '')

        if (isForbidden(arg.value)) {
          context.report({
            node,
            message: `${packageName} is bloat`,
          })
        }
      }
    },
  }
}
