const requireIndex = require('requireindex')

exports.configs = {
  recommended: {
    rules: {
      'mary/no-split-last': 'error',
      'mary/no-bloat': 'error',
    },
  },
}

exports.rules = requireIndex(`${__dirname}/rules`)
